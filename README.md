# Voltsig

Generate “visual signatures” of binary data that can be used to easily
differentiate up to 67.4 bits.

[Demo](https://mattereum-public.gitlab.io/voltsig/)

Copyright 2021 Alistair Turnbull. Distributed under the MIT licence; see the
file `LICENCE`.

The signatures are generated in colour. The colours used are
indistinguishable when rendered as greyscale, and the signatures are
suitable for users with colour vision deficiency (aka colour blindness);
however, viewing the images in colour gives an additional 9 bits of entropy.

Only the file `voltsig.js` is needed. It defines a single top-level
function:

+ `voltsig(svgElement, s, options)` — Replace the contents of the `<svg>`
  element `svgElement` with an image that represents a hash of `s`.

See `voltsig.js` for more detail.

Other files:

+ `index.html` — An example of how to use `voltsig.js`.
+ `sheep-face.svg` — The default logo (a tiny SVG file).
